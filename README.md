Exemple de json pour Postman

//création article
// POST : http://localhost:8080/articles

{ 
    "designation": "Nom de l'article", 
    "prix": 10.99, 
    "quantite": 5, 
    "commandes": []
}

// création commande
// POST : http://localhost:8080/commandes

{ 
    "total": 0,
    "articles": []
}


//Mise à jour de la commande
// POST : http://localhost:8080/commandes/1/articles/1

{ 
    "total": 0,
    "articles": [
        { 
            "designation": "Nom de l'article", 
            "prix": 10.99, 
            "quantite": 5, 
            "commandes": []
     }
    ]
}

