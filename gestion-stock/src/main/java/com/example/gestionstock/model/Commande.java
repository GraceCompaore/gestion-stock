package com.example.gestionstock.model;

import lombok.*;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name="commandes")
public class Commande {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer numero;

    @Column(nullable = false)
    private float total;

    @ManyToMany(mappedBy = "commandes", fetch = FetchType.LAZY)
    private List<Article> articles;

    public void ajouterArticle(Article article) {
        if (article.getQuantite() > 0) {
            article.setQuantite(article.getQuantite() - 1);
            articles.add(article);
            recalculerTotal();
        }
    }

    public void recalculerTotal() {
        float total = 0.0f;
        for (Article article : articles) {
            total += article.getPrix() * article.getQuantite();
        }
        this.total = total;
    }
}
