package com.example.gestionstock.model;

import lombok.*;

import jakarta.persistence.*;

import java.util.List;


@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="articles")
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 255)
    private String designation;

    @Column(nullable = false)
    private float prix;

    private Integer quantite;

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "Article_Commande",
            joinColumns = {@JoinColumn(name = "article_id")},
            inverseJoinColumns = {@JoinColumn(name = "commande_id")}
    )
    private List<Commande> commandes;

}

