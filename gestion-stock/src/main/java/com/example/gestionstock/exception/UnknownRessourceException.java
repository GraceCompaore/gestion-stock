package com.example.gestionstock.exception;

public class UnknownRessourceException extends RuntimeException {

    public UnknownRessourceException(String message) {
        super(message);
    }
}
