package com.example.gestionstock.service;

import com.example.gestionstock.model.Article;

import java.util.List;

public interface ArticleService {

    Article createArticle(Article article);

    List<Article> getAll();

    Article getById(Integer id);

    void delete(Integer id);

    Article update(Article article);

    //void decreaseQuantity(Article article);

    //void increaseQuantity(Article article, int quantite);
}
