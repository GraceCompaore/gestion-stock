package com.example.gestionstock.service;

import com.example.gestionstock.model.Article;
import com.example.gestionstock.model.Commande;

import java.util.List;

public interface CommandeService {
    Commande createCommande(Commande commande);

    List<Commande> getAll();

    Commande getById(Integer id);

    void delete(Integer id);

    void ajouterArticle(Integer commandeId, Integer articleId);

    List<Commande> getCommandeArticle(Integer articleId);

}
