package com.example.gestionstock.service.impl;

import com.example.gestionstock.exception.UnknownRessourceException;
import com.example.gestionstock.model.Article;
import com.example.gestionstock.model.Commande;
import com.example.gestionstock.repository.ArticleRepository;
import com.example.gestionstock.repository.CommandeRepository;
import com.example.gestionstock.service.CommandeService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CommandeServiceImpl implements CommandeService {

    private final CommandeRepository commandeRepository;

    private final ArticleRepository articleRepository;

    @Override
    public Commande createCommande(Commande commande) {
        this.commandeRepository.save(commande);
        commande.recalculerTotal();
        return commande;
    }

    @Override
    public List<Commande> getAll() {
        return this.commandeRepository.findAll();
    }

    @Override
    public Commande getById(Integer id) {
        return this.commandeRepository
                .findById(id)
                .orElseThrow(() -> new UnknownRessourceException(("No command found for the given ID")));
    }

    @Override
    public void delete(Integer id) {
       Commande commandeToDelete = this.getById(id);
       this.commandeRepository.delete(commandeToDelete);
    }

    @Override
    public void ajouterArticle(Integer commandeId, Integer articleId) {
        Commande commande = getById(commandeId);
        Article article = articleRepository.findById(articleId)
                .orElseThrow(() -> new UnknownRessourceException("No article found for the given ID"));

        commande.ajouterArticle(article);
        articleRepository.save(article);

        commande.recalculerTotal();
        commandeRepository.save(commande);
    }

    @Override
    public List<Commande> getCommandeArticle(Integer articleId) {
        Article article = articleRepository.findById(articleId).orElseThrow(() -> new UnknownRessourceException("Article non trouvé"));
        return article.getCommandes();
    }


}
