package com.example.gestionstock.service.impl;

import com.example.gestionstock.exception.UnknownRessourceException;
import com.example.gestionstock.model.Article;
import com.example.gestionstock.repository.ArticleRepository;
import com.example.gestionstock.service.ArticleService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ArticleServiceImpl implements ArticleService {

    Logger log= LoggerFactory.getLogger(ArticleService.class);

    private final ArticleRepository articleRepository;

    @Override
    public Article createArticle(Article article) {
        log.debug("Tentative d'enregistrement de l'article dans la base de données...");
        return this.articleRepository.save(article);
    }

    @Override
    public List<Article> getAll() {
        return this.articleRepository.findAll(Sort.by("id").ascending());
    }

    @Override
    public Article getById(Integer id) {
        return articleRepository
                .findById(id)
                .orElseThrow(() -> new UnknownRessourceException("Auncun article trouvé à cet ID"));
    }

    @Override
    public void delete(Integer id) {
        Article articleToDelete = this.getById(id);
        this.articleRepository.delete(articleToDelete);
    }

    @Override
    public Article update(Article article) {
        log.debug("Tentative de mise à jour de l'article {}", article.getId());
        Article existingArticle = this.getById(article.getId());
        existingArticle.setDesignation(article.getDesignation());
        existingArticle.setPrix(article.getPrix());
        existingArticle.setQuantite(article.getQuantite());
        return this.articleRepository.save(existingArticle);
    }

    /*@Override
    public void  decreaseQuantity(Article article) {
        Article existingArticle = articleRepository.findById(article.getId()).orElse(null);

        if (existingArticle != null) {
            int newQuantite = existingArticle.getQuantite() - 1;

            if (newQuantite >= 0) {
                // Si la quantité après le retrait est supérieure ou égale à zéro, mettre à jour la quantité
                existingArticle.setQuantite(newQuantite);
                articleRepository.save(existingArticle);
            } else {
                // Si la quantité après le retrait devient négative, vous pouvez gérer l'erreur ou effectuer une autre action
                throw new IllegalArgumentException("La quantité après le retrait devient négative.");
            }
        } else {
            // Gérer le cas où l'article n'existe pas
            throw new IllegalArgumentException("L'article spécifié n'existe pas dans le stock.");
        }

    }*/

    /*@Override
    public void increaseQuantity(Article article, int quantiteAjoute) {
        Article existingArticle = articleRepository.findById(article.getId()).orElse(null);

        if (existingArticle != null) {
            // Si l'article existe déjà, augmenter la quantité
            existingArticle.setQuantite(existingArticle.getQuantite() + quantiteAjoute);
            articleRepository.save(existingArticle);
        } else {
            // Si l'article n'existe pas, créer un nouvel article avec la quantité spécifiée
            article.setQuantite(quantiteAjoute);
            articleRepository.save(article);
        }
    }*/
}
