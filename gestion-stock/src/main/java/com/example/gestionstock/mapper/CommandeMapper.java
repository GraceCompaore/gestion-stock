package com.example.gestionstock.mapper;

import com.example.gestionstock.api.dto.CommandeDto;
import com.example.gestionstock.model.Commande;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface CommandeMapper {

    CommandeDto mapToDto(Commande commande);

    Commande mapToModel(CommandeDto commandeDto);
}
