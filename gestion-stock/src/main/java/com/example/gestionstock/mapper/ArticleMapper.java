package com.example.gestionstock.mapper;


import com.example.gestionstock.api.dto.ArticleDto;
import com.example.gestionstock.model.Article;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface ArticleMapper {

    ArticleDto mapToDto(Article article);

    Article mapToModel(ArticleDto articleDto);
}
