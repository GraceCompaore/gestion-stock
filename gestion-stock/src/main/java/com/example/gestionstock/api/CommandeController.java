package com.example.gestionstock.api;

import com.example.gestionstock.api.dto.CommandeDto;
import com.example.gestionstock.exception.UnknownRessourceException;
import com.example.gestionstock.mapper.CommandeMapper;
import com.example.gestionstock.service.CommandeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/commandes", produces = APPLICATION_JSON_VALUE)
public class CommandeController {

    private final CommandeService commandeService;
    private final CommandeMapper commandeMapper;

    public CommandeController(CommandeService commandeService, CommandeMapper commandeMapper) {
        this.commandeService = commandeService;
        this.commandeMapper = commandeMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<CommandeDto>> getAll() {
        return ResponseEntity.ok(
                this.commandeService.getAll().stream()
                        .map(this.commandeMapper::mapToDto)
                        .toList()
        );
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CommandeDto> getById(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(this.commandeMapper
                    .mapToDto(this.commandeService.getById(id)));
        } catch (UnknownRessourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }

    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    ResponseEntity<CommandeDto> create(@RequestBody final CommandeDto commandeDto) {
        CommandeDto commandeDtoResponse =
                this.commandeMapper.mapToDto(
                        this.commandeService.createCommande(
                                this.commandeMapper.mapToModel(commandeDto)
                        ));
        return ResponseEntity.created(URI.create("/commandes" + commandeDtoResponse.getNumero()))
                .body(commandeDtoResponse);
    }


    @DeleteMapping(path = "/{id}")

    public ResponseEntity<Void> delete(@PathVariable final Integer id) {
        try {
            this.commandeService.delete(id);
            return ResponseEntity.noContent().build();
        } catch (UnknownRessourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }

    @PostMapping("/{commandeId}/articles/{articleId}")
    public ResponseEntity<String> ajouterArticle(@PathVariable("commandeId") Integer commandeId, @PathVariable("articleId") Integer articleId) {
        try {
            commandeService.ajouterArticle(commandeId, articleId);
            return ResponseEntity.ok("Article ajouté à la commande avec succès.");
        } catch (UnknownRessourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }

    @GetMapping(value = "/articles/{articleId}")
    public ResponseEntity<List<CommandeDto>> getCommandeArticle(@PathVariable Integer articleId) {
        try {
            return ResponseEntity.ok(this.commandeService.getCommandeArticle(articleId).stream()
                    .map(commandeMapper::mapToDto)
                    .toList());
        } catch (UnknownRessourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }


}
