package com.example.gestionstock.api.dto;

import com.example.gestionstock.model.Article;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommandeDto {

    private Integer numero;
    private float total;
    private List<ArticleMinimalDto> articles;
}
