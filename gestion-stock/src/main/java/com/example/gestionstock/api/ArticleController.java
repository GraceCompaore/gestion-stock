package com.example.gestionstock.api;

import com.example.gestionstock.api.dto.ArticleDto;
import com.example.gestionstock.exception.UnknownRessourceException;
import com.example.gestionstock.mapper.ArticleMapper;
import com.example.gestionstock.service.ArticleService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/articles", produces = APPLICATION_JSON_VALUE)
public class ArticleController {
    private final ArticleService articleService;
    private final ArticleMapper articleMapper;

    public ArticleController(ArticleService articleService, ArticleMapper articleMapper) {
        this.articleService = articleService;
        this.articleMapper = articleMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<ArticleDto>> getAll() {
        return ResponseEntity.ok(
                this.articleService.getAll().stream()
                        .map(this.articleMapper::mapToDto)
                        .toList()
        );
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<ArticleDto> getById(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(this.articleMapper
                    .mapToDto(this.articleService.getById(id)));
        } catch (UnknownRessourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }

    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    ResponseEntity<ArticleDto> create(@RequestBody final ArticleDto articleDto) {
        ArticleDto articleDtoResponse =
                this.articleMapper.mapToDto(
                        this.articleService.createArticle(
                                this.articleMapper.mapToModel(articleDto)
                        ));
        return ResponseEntity.created(URI.create("/articles" + articleDtoResponse.getId()))
                .body(articleDtoResponse);
    }


    @DeleteMapping(path = "/{id}")

    public ResponseEntity<Void> delete(@PathVariable final Integer id) {
        try {
            this.articleService.delete(id);
            return ResponseEntity.noContent().build();
        } catch (UnknownRessourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }

    @PutMapping(path = "/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE})
    public ResponseEntity<Void> update(@PathVariable final Integer id, @RequestBody ArticleDto articleDto) {
        try {
            articleDto.setId(id);
            this.articleService.update(articleMapper.mapToModel(articleDto));

            return ResponseEntity.noContent().build();
        }catch (UnknownRessourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }

}
